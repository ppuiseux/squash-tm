# Déployer Squash TM en multi-noeuds sur Kubernetes et consorts

**Ne pas reprendre tel quel le code ci-dessous, il sert juste d'inspiration !**
**Utiliser les fichier du projet**

*Permet de déployer des mises à jjour de version mineures sans interruption de service autre qu'une déconnexion de session utilisateur*
*Permet aussi d'accepter une charge plus importante (plus d'utilisateur) sur une même instance Squash TM avec une unique URL d'accès*

Configuration inspirée du lien suivant : https://stackoverflow.com/questions/67212207/kubernetes-on-premises-metallb-loadbalancer-and-sticky-sessions

Déployé sur Minikube version v1.25.2, commit: 362d5fdc0a3dbee389b3d3f1034e8023e72bd3a7 sur Windows 10 et HyperV

Utilise une base PostgreSQL mais doit surement fonctionner avec MariaDB, mais pas H2, sauf si vous centralisez le stockage du fichier de BDD H2 (à tester, mais assez perilleux pour les acces concurentiels au fichier).

Liste des addons de mon Minikube

|-----------------------------|----------|--------------|--------------------------------|
|         ADDON NAME          | PROFILE  |    STATUS    |           MAINTAINER           |
|-----------------------------|----------|--------------|--------------------------------|
| ambassador                  | minikube | disabled     | third-party (ambassador)       |
| auto-pause                  | minikube | disabled     | google                         |
| csi-hostpath-driver         | minikube | disabled     | kubernetes                     |
| dashboard                   | minikube | enabled ✅   | kubernetes                     |
| default-storageclass        | minikube | enabled ✅   | kubernetes                     |
| efk                         | minikube | disabled     | third-party (elastic)          |
| freshpod                    | minikube | disabled     | google                         |
| gcp-auth                    | minikube | disabled     | google                         |
| gvisor                      | minikube | disabled     | google                         |
| helm-tiller                 | minikube | disabled     | third-party (helm)             |
| ingress                     | minikube | enabled ✅   | unknown (third-party)          |
| ingress-dns                 | minikube | enabled ✅   | google                         |
| istio                       | minikube | disabled     | third-party (istio)            |
| istio-provisioner           | minikube | disabled     | third-party (istio)            |
| kong                        | minikube | disabled     | third-party (Kong HQ)          |
| kubevirt                    | minikube | disabled     | third-party (kubevirt)         |
| logviewer                   | minikube | disabled     | unknown (third-party)          |
| metallb                     | minikube | disabled     | third-party (metallb)          |
| metrics-server              | minikube | disabled     | kubernetes                     |
| nvidia-driver-installer     | minikube | disabled     | google                         |
| nvidia-gpu-device-plugin    | minikube | disabled     | third-party (nvidia)           |
| olm                         | minikube | disabled     | third-party (operator          |
|                             |          |              | framework)                     |
| pod-security-policy         | minikube | disabled     | unknown (third-party)          |
| portainer                   | minikube | disabled     | portainer.io                   |
| registry                    | minikube | disabled     | google                         |
| registry-aliases            | minikube | disabled     | unknown (third-party)          |
| registry-creds              | minikube | disabled     | third-party (upmc enterprises) |
| storage-provisioner         | minikube | enabled ✅   | google                         |
| storage-provisioner-gluster | minikube | disabled     | unknown (third-party)          |
| volumesnapshots             | minikube | disabled     | kubernetes                     |
|-----------------------------|----------|--------------|--------------------------------|

## Kube Proxy (**À faire absolument**)
`$ kubectl edit configmap -n kube-system kube-proxy`

    apiVersion: kubeproxy.config.k8s.io/v1alpha1 
    kind: KubeProxy
    Configuration mode:
    "ipvs" ipvs:
       strictARP: true # Non ajouté, et ça fonctionne sans

    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/main/manifests/namespace.yaml

    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/main/manifests/metallb.yaml
    
    kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
*Stocker le resultat de la variable ci dessus dans une varaiable PS sur Windows si pas de WSL*

**Ce qui suit est pour information/inspiration**

## Config-map
`$ vim config-map.yaml`

    apiVersion: v1
    kind: ConfigMap
    metadata:
      namespace: metallb-system
      name: config
    data:
      config: |
        address-pools:
        - name: default
          protocol: layer2
          addresses:
          - 10.100.170.200-10.100.170.220

`$ kubectl apply -f config-map.yaml`

`$ kubectl describe configmap config -n metallb-system`

## Déploiement
`$ vim myapp-tst-deploy.yaml`

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: myapp-tst-deployment
      labels:
        app: myapp-tst
    spec:
      replicas: 2
      selector:
        matchLabels:
          app: myapp-tst
      template:
        metadata:
          labels:
            app: myapp-tst
        spec:
          containers:
          - name: myapp-tst
            image: myapp-tomcat
            securityContext:
              privileged: true
              capabilities:
                add:
                  - SYS_ADMIN

## Service
`$ vim myapp-tst-service.yaml`

    apiVersion: v1
    kind: Service
    metadata:
      name: myapp-tst-service
      labels:
        app: myapp-tst
    spec:
      externalTrafficPolicy: Cluster
      type: LoadBalancer
      ports:
      - name: myapp-tst-port
        nodePort: 30080
        port: 80
        protocol: TCP
        targetPort: 8080
      selector:
        app: myapp-tst
      sessionAffinity: ClientIP

## Ingress
`$ vim myapp-tst-ingress.yaml`

    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
      name: myapp-tst-ingress
      annotations:
        kubernetes.io/ingress.class: "nginx"
        nginx.ingress.kubernetes.io/affinity: "cookie"
        nginx.ingress.kubernetes.io/affinity-mode: "persistent"
        nginx.ingress.kubernetes.io/session-cookie-name: "INGRESSCOOKIE"
        nginx.ingress.kubernetes.io/session-cookie-expires: "172800"
        nginx.ingress.kubernetes.io/session-cookie-max-age: "172800"
    spec:
      rules: 
        - http:
            paths:
              - path: /
                backend:
                  serviceName: myapp-tst-service
                  servicePort: myapp-tst-port

I run `kubectl -f apply` for all three files, and these is my result:

`$ kubectl get all -o wide`

    NAME                                     READY   STATUS    RESTARTS   AGE     IP          NODE               NOMINATED NODE   READINESS GATES
    pod/myapp-tst-deployment-54474cd74-p8cxk   1/1     Running   0          4m53s   10.36.0.1   bcc-tst-docker02   <none>           <none>
    pod/myapp-tst-deployment-54474cd74-pwlr8   1/1     Running   0          4m53s   10.44.0.2   bca-tst-docker01   <none>           <none>

    NAME                      TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)        AGE     SELECTOR
    service/myapp-tst-service   LoadBalancer   10.110.184.237   10.100.170.15   80:30080/TCP   4m48s   app=myapp-tst,tier=backend
    service/kubernetes        ClusterIP      10.96.0.1        <none>          443/TCP        6d22h   <none>

    NAME                                 READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES                  SELECTOR
    deployment.apps/myapp-tst-deployment   2/2     2            2           4m53s   myapp-tst      mferraramiki/myapp-test   app=myapp-tst

    NAME                                           DESIRED   CURRENT   READY   AGE     CONTAINERS   IMAGES                  SELECTOR
    replicaset.apps/myapp-tst-deployment-54474cd74   2         2         2       4m53s 
